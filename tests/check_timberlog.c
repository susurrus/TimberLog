#include <stdio.h>
#include <stdlib.h>

#include <check.h>

#include "CircularBuffer.h"
#include "Config.h"

START_TEST (empty_config)
{
    ConfigParams params = {};
    const char empty_test[] = "";
    for (int i = 0; i < sizeof(empty_test); ++i) {
        ck_assert(ProcessConfigFile(empty_test[i], &params));
    }
}
END_TEST

START_TEST (comment_config)
{
    ConfigParams params = {};
    const char comment_test[] = "#";
    for (int i = 0; i < sizeof(comment_test); ++i) {
        ck_assert(ProcessConfigFile(comment_test[i], &params));
    }
}
END_TEST

START_TEST (comment_newline_config)
{
    ConfigParams params = {};
    const char test_str[] = "#\n";
    for (int i = 0; i < sizeof(test_str); ++i) {
        ck_assert(ProcessConfigFile(test_str[i], &params));
    }
}
END_TEST

START_TEST (uart1_input)
{
    ConfigParams params = {};
    const char uart1_test[] = "uart1_input b2";
    for (int i = 0; i < sizeof(uart1_test); ++i) {
        ck_assert(ProcessConfigFile(uart1_test[i], &params));
    }
    ck_assert(params.uart1Input == PIN_B2);
}
END_TEST

START_TEST (uart1_baud)
{
    ConfigParams params = {};
    const char test_str[] = "uart1_baud 115200";
    for (int i = 0; i < sizeof(test_str); ++i) {
        ck_assert(ProcessConfigFile(test_str[i], &params));
    }
    ck_assert_uint_eq(params.uart1BaudRate, 115200ul);
}
END_TEST

START_TEST (uart1_baud_newline)
{
    ConfigParams params = {};
    const char test_str[] = "uart1_baud 115200\n";
    for (int i = 0; i < sizeof(test_str); ++i) {
        ck_assert(ProcessConfigFile(test_str[i], &params));
    }
    ck_assert_uint_eq(params.uart1BaudRate, 115200ul);
}
END_TEST

START_TEST (uart1_config)
{
    ConfigParams params = {};
    const char test_str[] = "uart1_baud 115200\nuart1_input b2";
    for (int i = 0; i < sizeof(test_str); ++i) {
        ck_assert(ProcessConfigFile(test_str[i], &params));
    }
    ck_assert_uint_eq(params.uart1BaudRate, 115200ul);
    ck_assert_uint_eq(params.uart1Input, PIN_B2);
}
END_TEST

/**
 * Just check that parsing the example file is completely successful. We
 * don't actually check any values here as those are all checked for successful
 * parsing above.
 */
START_TEST (example_file)
{
    // Open the example file
    FILE *f = fopen("../config.txt", "r");
    ck_assert(f);

    // Scan all contents of the file
    ConfigParams params = {};
    while (true) {
        char c = fgetc(f);
        if (feof(f)) {
            break;
        }
        ck_assert(ProcessConfigFile(c, &params));
    }
    fclose(f);
}
END_TEST

/**
 * @brief A struct used for testing.
 */
typedef struct {
    uint8_t hey;
    int foo;
    float bar;
} TestStruct;

/**
 * @brief Returns whether two circular buffers are equal in their metadata.
 *
 * This does not do an exact comparison of their data arrays, just their metadata.
 */
int TestStructEqual(const TestStruct *a, const TestStruct *b)
{
    return (a->hey == b->hey &&
            a->foo == b->foo &&
            a->bar == b->bar);
}

/*
 * These tests check the ability of the circular buffer to write a single item and
 * then read it back.
 */
START_TEST (circularbuffer_simple)
{
    // Create a new circular buffer.
    CircularBuffer b;
    uint16_t size = 256;
    uint8_t *buffer = (uint8_t*)malloc(256*sizeof(uint8_t));
    CB_Init(&b, buffer, size);
    ck_assert(!b.dataSize);

    // Add a single item and check.
    CB_WriteByte(&b, 0x56);
    ck_assert(b.dataSize == 1);
    uint8_t peekval;
    ck_assert(CB_Peek(&b, &peekval));
    ck_assert_int_eq(peekval, 0x56);


    // Remove that item and check.
    uint8_t d;
    ck_assert(CB_ReadByte(&b, &d));
    ck_assert_int_eq(d, 0x56);
    ck_assert_int_eq(b.dataSize, 0);
    ck_assert_int_eq(CB_Peek(&b, &peekval), 0);

    free(buffer);
}
END_TEST

/*
 * This tests the ability of the buffer to read and write many items. This code also tests
 * writing to and reading from a buffer which has been wrapped around. PeekMany() is tested.
 */
START_TEST (circularbuffer_complex1)
{
    // Create a new circular buffer.
    CircularBuffer b;
    uint16_t size = 256;
    uint8_t *buffer = (uint8_t*)malloc(256*sizeof(uint8_t));
    CB_Init(&b, buffer, size);
    ck_assert(!b.dataSize);

    // Here we make a 1016 int8_t long string for testing. Testing with the library with BUFFER_SIZE
    // set to larger than 1016 will produce errors.
    int8_t testString[] = "Copyright (C) <year> <copyright holders> Permission is hereby granted, free of int8_tge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THEAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";
    // Fill the buffer to SIZE+1 and check.
    uint16_t i;
    for (i = 0; i < b.staticSize; ++i) {
        ck_assert(b.dataSize == i);
        ck_assert(CB_WriteByte(&b, testString[i]));
        ck_assert(b.dataSize == i + 1);
    }
    ck_assert(!CB_WriteByte(&b, 0x89));
    ck_assert(b.overflowCount == 1);


    // Run a deepPeek on the now-full buffer.
    uint8_t tmpString[b.staticSize];
    ck_assert(CB_PeekMany(&b, tmpString, b.staticSize));
    ck_assert(b.dataSize == b.staticSize);


    i = 0;
    while (i < 256) {
        ++i;
    }
    ck_assert(memcmp(testString, tmpString, b.staticSize) == 0);
    ck_assert(b.dataSize);

    // Verify reading of an entire circular-buffer
    uint8_t d;
    i = b.dataSize;
    while (i > 0) {
    ck_assert(CB_ReadByte(&b, &d));
    ck_assert(d == testString[b.staticSize - i]);
        i--;
    }
    ck_assert(b.dataSize == 0);
    d = 0x77;
    int8_t d2 = d;
    ck_assert(!CB_ReadByte(&b, &d));
    ck_assert(d == d2); //nothing has been read

    // Test element insertion when the buffer wraps around.
    uint8_t peekval;
    ck_assert(CB_WriteByte(&b, 91));
    ck_assert(b.overflowCount == 1); // Overflow is triggered on an earlier test
    ck_assert(b.dataSize == 1);
    CB_Peek(&b, &peekval);
    ck_assert(peekval == 91);
    ck_assert(CB_WriteByte(&b, 92));
    ck_assert(b.dataSize == 2);
    ck_assert(CB_WriteByte(&b, 93));
    ck_assert(b.dataSize == 3);
    ck_assert(CB_WriteByte(&b, 94));
    ck_assert(b.dataSize == 4);

    // Test PeekMany on wrapped-around buffers
    uint8_t peekData[4];
    ck_assert(CB_PeekMany(&b, peekData, 4));
    ck_assert(peekData[0] == 91);
    ck_assert(peekData[1] == 92);
    ck_assert(peekData[2] == 93);
    ck_assert(peekData[3] == 94);

    // Test reading now.
    ck_assert(CB_ReadByte(&b, &d) && d == 91);
    ck_assert(CB_ReadByte(&b, &d) && d == 92);
    ck_assert(CB_ReadByte(&b, &d) && d == 93);
    ck_assert(CB_ReadByte(&b, &d) && d == 94);
    ck_assert(!CB_ReadByte(&b, &d) && d == 94);

    free(buffer);
}
END_TEST

/*
 * This section of test code checks that CB_Init will not initialize a buffer if its
 * arguments are not valid.
 */
START_TEST (circularbuffer_init)
{
    //Test initialization with invalid arguments
    CircularBuffer b;
    uint8_t *buffer = (uint8_t*)malloc(256*sizeof(uint8_t));
    ck_assert(CB_Init(&b, buffer, 0) == false); //checks the invalid argument size = 0
    ck_assert(CB_Init(&b, buffer, 1) == false); //checks the invalid argument size = 1
    ck_assert(CB_Init(&b, buffer, 128) == true); //checks that the function returns true upon true
    free(buffer);
    buffer = NULL;
    ck_assert(CB_Init(&b, buffer, 16) == false); //tests the invalid argument where buffer is a null pointer
}
END_TEST

/*
 * This code tests the buffer at the edge case size is two.
 */
START_TEST (circularbuffer_size_2)
{
    //Test functionality at edge case size is 2
    uint8_t CBtestbuf[2];
    CircularBuffer b;
    CB_Init(&b, CBtestbuf, 2);  //creates a new buffer of length two
    ck_assert(!b.dataSize);

    // Add a single item and check.
    uint8_t peekval;
    CB_WriteByte(&b, 0x56);
    ck_assert(b.dataSize == 1);
    CB_Peek(&b, &peekval);
    ck_assert(peekval == 0x56);

    // Remove that item and check.
    uint8_t e;
    ck_assert(CB_ReadByte(&b, &e) && e == 0x56);
    ck_assert(b.dataSize == 0);
    ck_assert(CB_Peek(&b, &peekval) == 0);

        //Now write two characters to the buffer
    ck_assert(CB_WriteByte(&b, 0x56));
    ck_assert(CB_WriteByte(&b, 0x58));
    ck_assert(b.dataSize == 2); //Check to see if the length is correct
    CB_WriteByte(&b, 0x59);  //Write a third element to the two bit buffer
    ck_assert(b.overflowCount == 1); //Check that overflow has occurred
    ck_assert(CB_ReadByte(&b, &e) && e == 0x56); //Check Reading an element
    ck_assert(b.dataSize == 1); //Check the length of the buffer
    ck_assert(CB_ReadByte(&b, &e) && e == 0x58); //Check Reading an element
    ck_assert(b.dataSize == 0); //Check that the buffer is now empty
    ck_assert(CB_ReadByte(&b, &e) == false); //checks that the empty buffer cannot be read from
}
END_TEST

/*
 * This code tests the edge case where the buffer is at the maximum size.  The size
 * is limited by the maximum value which can be held in a uint16_t.
 */
START_TEST (circularbuffer_size_max)
{
    //Test functionality at edge case size is UINT16_MAX
    CircularBuffer b;
    uint8_t *buffertwo = (uint8_t*)malloc(UINT16_MAX*sizeof(uint8_t));

    CB_Init(&b, buffertwo, UINT16_MAX);
    ck_assert(!b.dataSize);
    ck_assert(b.staticSize == UINT16_MAX);


    // Here we use the same UINT16_MAX int8_character long string for testing. Testing with the library with BUFFER_SIZE
    // set to larger than UINT16_MAX will produce errors.
    uint8_t testStringtwo[b.staticSize+1];

    int i;
    for (i = 0; i < b.staticSize; ++i) {
        testStringtwo[i] = i;
    }

    // Fill the buffer to SIZE+1 and check
    for (i = 0; i < b.staticSize; ++i) {
        ck_assert(b.dataSize == i);
        ck_assert(CB_WriteByte(&b, i));
        ck_assert(b.dataSize == i + 1);
    }
    ck_assert(!CB_WriteByte(&b, 0x89));
    ck_assert(b.overflowCount == 1);

    // Run a deepPeek on the now-full buffer.
    uint8_t tmpStringtwo[b.staticSize];
    ck_assert(CB_PeekMany(&b, tmpStringtwo, b.staticSize));
    ck_assert(b.dataSize == b.staticSize);
    ck_assert(memcmp(testStringtwo, tmpStringtwo, b.staticSize) == 0);

    // Verify reading of an entire circular-buffer
    uint8_t d;
    for (i = b.dataSize; i > 0; --i) {
        ck_assert(CB_ReadByte(&b, &d));
        ck_assert(d == testStringtwo[b.staticSize - i]);
    }
    ck_assert(b.dataSize == 0);
    d = 0x77;
    int8_t d3 = d;
    ck_assert(!CB_ReadByte(&b, &d));
    ck_assert(d == d3);
    ck_assert(!b.dataSize);

    // Test element insertion when the buffer wraps around.
    uint8_t peekval;
    ck_assert(CB_WriteByte(&b, 91));
    ck_assert(b.overflowCount == 1); // Overflow is triggered on an earlier test
    ck_assert(b.dataSize == 1);
    CB_Peek(&b, &peekval);
    ck_assert(peekval == 91);
    ck_assert(CB_WriteByte(&b, 92));
    ck_assert(b.dataSize == 2);
    ck_assert(CB_WriteByte(&b, 93));
    ck_assert(b.dataSize == 3);
    ck_assert(CB_WriteByte(&b, 94));
    ck_assert(b.dataSize == 4);

    // Test DeepPeek on wrapped-around buffers
    uint8_t peekDatatwo[4];
    ck_assert(CB_PeekMany(&b, peekDatatwo, 4));
    ck_assert(peekDatatwo[0] == 91);
    ck_assert(peekDatatwo[1] == 92);
    ck_assert(peekDatatwo[2] == 93);
    ck_assert(peekDatatwo[3] == 94);

    // Test reading now.
    ck_assert(CB_ReadByte(&b, &d) && d == 91);
    ck_assert(CB_ReadByte(&b, &d) && d == 92);
    ck_assert(CB_ReadByte(&b, &d) && d == 93);
    ck_assert(CB_ReadByte(&b, &d) && d == 94);
    ck_assert(!CB_ReadByte(&b, &d) && d == 94);

    free(buffertwo);
}
END_TEST

/*
 * This tests the remove function
 */
START_TEST (circularbuffer_remove)
{
    CircularBuffer b;
    uint8_t CBtestbuften[10];
    CB_Init(&b, CBtestbuften, 10);  //creates a new buffer of length ten
    ck_assert(!b.dataSize);

    int i;
    i = 0;
    while(i < 9){
        CB_WriteByte(&b, i);
        ++i;
    }
    //Test removing a valid number of items
    ck_assert(b.dataSize == 9);
    ck_assert(CB_Remove(&b, 4));
    ck_assert(b.dataSize == 5);
    uint8_t d;
    CB_ReadByte(&b, &d);
    ck_assert(d == 4);

    //Test removing more items than are in the buffer
    ck_assert(b.dataSize == 4);
    CB_Remove(&b, 10);
    ck_assert(b.dataSize == 0); //The buffer is now empty
}
END_TEST

/*
 * This tests using the CB_ReadMany function to read a buffer.
 */
START_TEST (circularbuffer_readmany)
{
    /**Test reading multiple values from the buffer using CB_ReadMany */
    CircularBuffer b;
    uint8_t CBtestbufthirty[30];
    CB_Init(&b, CBtestbufthirty, 30);  //creates a new buffer of length thirty
    ck_assert(!b.dataSize);

    int i = 0;
    while(i < 30){
        CB_WriteByte(&b, i);
        ++i;
    }
    ck_assert(b.dataSize == 30);

    uint8_t readresults[30];
    ck_assert(CB_ReadMany(&b,readresults, 15));
    i = 0;
    while(i < 15){
    ck_assert(readresults[i] == i);
        ++i;
    }
    ck_assert(b.dataSize == 15);
    ck_assert(CB_ReadMany(&b,readresults, 15));
    ck_assert(b.dataSize == 0); //Checks that buffer is now empty
    ck_assert(b.readIndex == b.writeIndex); //checks that the pointers are equal
}
END_TEST

/* This tests using CB_WriteMany to write to a buffer.
*/
START_TEST (circularbuffer_writemany)
{
    /**Testing the WriteMany function*/
    CircularBuffer b;
    uint8_t CBtestbufthirty[30];
    CB_Init(&b, CBtestbufthirty, 30);  //re-initializes buffer of length thirty
    ck_assert(!b.dataSize);

    uint8_t readresults[100];
    int i = 0;
    while (i < 100) {
        readresults[i] = i;
        ++i;
    }
    ck_assert(CB_WriteMany(&b, readresults, 22, true)); //write 22 values from readresults to the buffer
    ck_assert(b.dataSize == 22);

    uint8_t d;
    i = 0;
    while (i < 22) {
        CB_ReadByte(&b, &d);
    ck_assert(d == i);
        ++i;
    }

    CB_Init(&b, CBtestbufthirty, 30);  //re-initializes buffer of length thirty
    ck_assert(!b.dataSize);
    ck_assert(b.readIndex == b.writeIndex);
    i = 0;
    while (i < 30) {
        readresults[i] = i;
        ++i;
    }
    CB_WriteMany(&b, readresults, 22, false); //write 22 values from readresults to the buffer
    ck_assert(b.dataSize == 22);
    i = 0;
    while (i < 22) {
        CB_ReadByte(&b, &d);
    ck_assert(d == i);
        ++i;
    }

    /**Now test the failure criteria specified in failEarly*/
    CB_Init(&b, CBtestbufthirty, 30);  //re-initializes buffer of length thirty
    CB_WriteMany(&b, readresults, 18, true);
    ck_assert(b.dataSize == 18);
    ck_assert(CB_WriteMany(&b, readresults, 50, true) == false); //Writing more than the buffer can hold returns an error
    ck_assert(b.dataSize == 18); // Checks that nothing was written
    ck_assert(!CB_WriteMany(&b, readresults, 100, false)); // Now without the size check
    ck_assert(b.dataSize == 30); //Checks that buffer is now full
    ck_assert(b.overflowCount == 88); //100-(30-18) = 88 elements have overflowed

    i = 0;
    while (i < 18) {
        CB_ReadByte(&b, &d);
    ck_assert(i == d);
        ++i;
    }
    while (i < 12) {
        CB_ReadByte(&b, &d);
    ck_assert(i == d);
        ++i;
    }
}
END_TEST

/* This code tests using the CB_WriteMany and CB_ReadMany functions to read and write
 *structures to a buffer.  CB_PeekMany is also tested.
 */
START_TEST (circularbuffer_read_write)
{
    // The test circular buffer
    CircularBuffer c;

    //Testing writing of a structure to the buffer and reading it back
    // Create a new circular buffer.
    TestStruct t1 = {6, 42, 1.5};

    uint16_t sizetwo = 256*sizeof(TestStruct);
    uint8_t structbuff[sizetwo];
    ck_assert(CB_Init(&c, structbuff, sizetwo)); //Creates a buffer to hold 256 TestStruct structures
    ck_assert(!c.dataSize);
    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    ck_assert(c.dataSize == sizeof(TestStruct));

    TestStruct f;
    CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the structure back from the buffer
    ck_assert(!c.dataSize); //the buffer is now empty
    ck_assert(TestStructEqual(&f, &t1));

    TestStruct t2 = {56, 700, 5.75}; //filled with arbitrary values

    //Write a single structure to the buffer and then read it back
    ck_assert(c.dataSize == 0);
    ck_assert(CB_WriteMany(&c, &t2, sizeof(TestStruct), true));
    ck_assert(c.dataSize == sizeof(TestStruct));

    ck_assert(CB_ReadMany(&c, &f, sizeof(TestStruct))); //read the structure back from the buffer
    ck_assert(TestStructEqual(&f, &t2));

    //Write two structures to the buffer and then read them back
    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t2, sizeof(TestStruct), true);

    ck_assert(c.dataSize == 2*sizeof(TestStruct));

    CB_ReadMany(&c, &f, sizeof(TestStruct)); //read first the structure back from the buffer
    ck_assert(TestStructEqual(&f, &t1));

    CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the second structure back from the buffer
    ck_assert(TestStructEqual(&f, &t2));

    ck_assert(c.readIndex == c.writeIndex); //The buffer is now empty.

    // Write four structures to the buffer using a loop and read them back.

    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t2, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t2, sizeof(TestStruct), true);

    ck_assert(c.dataSize == 4*sizeof(TestStruct));

    CB_ReadMany(&c, &f, sizeof(TestStruct)); //read first the structure back from the buffer
    ck_assert(TestStructEqual(&f, &t1));

    ck_assert(c.dataSize == 3*sizeof(TestStruct));

    CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the second structure back from the buffer
    ck_assert(TestStructEqual(&f, &t2));

    ck_assert(c.dataSize == 2*sizeof(TestStruct));

    CB_ReadMany(&c, &f, sizeof(TestStruct)); //read third the structure back from the buffer
    ck_assert(TestStructEqual(&f, &t1));

    ck_assert(c.dataSize == 1*sizeof(TestStruct));

    CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the fourth structure back from the buffer
    ck_assert(TestStructEqual(&f, &t2));

    ck_assert(c.dataSize == 0); //the buffer is now empty

    // Now write six structures and then read them off using a loop.
    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t2, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t2, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    CB_WriteMany(&c, &t2, sizeof(TestStruct), true);

    int i = 0;
    while (i < 3) {
        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read first the structure back from the buffer
        ck_assert(TestStructEqual(&f, &t1));

        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the second structure back from the buffer
        ck_assert(TestStructEqual(&f, &t2));

        ++i;
    }

    //Now write from a loop and read from a loop
    i = 0;
    while (i < 3) {
        CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
        CB_WriteMany(&c, &t2, sizeof(TestStruct), true);
        ++i;
    }

    i = 0;
    while (i < 3) {
        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read first the structure back from the buffer
        ck_assert(TestStructEqual(&f, &t1));

        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the second structure back from the buffer
        ck_assert(TestStructEqual(&f, &t2));

        ++i;
    }

    ck_assert(c.readIndex == c.writeIndex); //the buffer is empty

    //Now write 40 elements from a loop and read 40 elements from a loop
    i = 0;
    while (i < 20) {
        CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
        CB_WriteMany(&c, &t2, sizeof(TestStruct), true);
        ++i;
    }

    i = 0;
    while (i < 20) {
        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read first the structure back from the buffer
        ck_assert(TestStructEqual(&f, &t1));

        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the second structure back from the buffer
        ck_assert(TestStructEqual(&f, &t2));

        ++i;
    }

    //Now try to overfill a buffer and then read the structures back
    //C is length 256*sizeof(TestStruct) so we will try to write 260 structures
    ck_assert(c.readIndex == c.writeIndex); //the buffer is empty
    ck_assert(c.dataSize == 0);

    i = 0;
    while (i < 130) { //writes 260 structures to the buffer
        CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
        CB_WriteMany(&c, &t2, sizeof(TestStruct), true);
        ++i;
    }

    i = 0;
    while (i < 128) {
        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read first the structure back from the buffer
        ck_assert(TestStructEqual(&f, &t1));

        CB_ReadMany(&c, &f, sizeof(TestStruct)); //read the second structure back from the buffer
        ck_assert(TestStructEqual(&f, &t2));

        ++i;
    }

    ck_assert(c.readIndex == c.writeIndex); //the buffer is empty
    ck_assert(c.dataSize == 0);

    //Testing CB_PeekMany on structures
    TestStruct peekTest;

    CB_WriteMany(&c, &t1, sizeof(TestStruct), true);
    CB_PeekMany(&c, (uint8_t*)&peekTest, sizeof(TestStruct));
    ck_assert(TestStructEqual(&t1, &peekTest));
}
END_TEST

/**
 * Test another complicated set of operations.
 */
START_TEST (circularbuffer_complex2)
{
    CircularBuffer circBuf;
    unsigned char data[20];
    unsigned char testIn[20] = "Hey There This Test";
    unsigned char testOut[20];

    // Initialize the circular buffer
    ck_assert(CB_Init(&circBuf, data, 20));
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 0);

    // Write and read to the buffer multiple times
    CB_WriteMany(&circBuf, testIn, 20, true);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 0);

    CB_PeekMany(&circBuf, testOut, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 0);

    CB_Remove(&circBuf, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 0);

    ck_assert(!memcmp(testIn, testOut, 20));


    CB_WriteMany(&circBuf, testIn, 20, true);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 0);

    CB_PeekMany(&circBuf, testOut, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 0);

    CB_Remove(&circBuf, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 0);

    ck_assert(!memcmp(testIn, testOut, 20));

    //Test uneven data
    CB_WriteMany(&circBuf, testIn, 7, true);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 7);

    CB_PeekMany(&circBuf, testOut, 7);
    ck_assert(circBuf.readIndex == 0);
    ck_assert(circBuf.writeIndex == 7);

    CB_Remove(&circBuf, 7);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 7);
    ck_assert(circBuf.writeIndex == 7);

    //Test the full data again
    CB_WriteMany(&circBuf, testIn, 20, true);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 7);
    ck_assert(circBuf.writeIndex == 7);

    CB_PeekMany(&circBuf, testOut, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 7);
    ck_assert(circBuf.writeIndex == 7);

    CB_Remove(&circBuf, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 7);
    ck_assert(circBuf.writeIndex == 7);

    ck_assert(!memcmp(testIn, testOut, 20));


    CB_WriteMany(&circBuf, testIn, 20, true);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 7);
    ck_assert(circBuf.writeIndex == 7);

    CB_PeekMany(&circBuf, testOut, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 7);
    ck_assert(circBuf.writeIndex == 7);

    CB_Remove(&circBuf, 20);
    ck_assert(circBuf.readIndex == circBuf.writeIndex);
    ck_assert(circBuf.readIndex == 7);
    ck_assert(circBuf.writeIndex == 7);

    ck_assert(!memcmp(testIn, testOut, 20));
}
END_TEST

START_TEST (circularbuffer_complex3)
{
    // 10 April 2013
    #define BUF_SIZE 512
    CircularBuffer circBuf;
    unsigned char data[BUF_SIZE*3];
    unsigned char testIn[BUF_SIZE];
    unsigned char testOut[BUF_SIZE];

    int i;
    for (i = 0; i<BUF_SIZE; i++) {
        testIn[i] = (char)i;
    }

    for (i = 0; i<10000; i++) {
        // Initialize the circular buffer
        ck_assert(CB_Init(&circBuf, data, 20));

        CB_WriteMany(&circBuf, testIn, BUF_SIZE, true);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);
        CB_PeekMany(&circBuf, testOut, BUF_SIZE);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);
        CB_Remove(&circBuf, BUF_SIZE);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);

        CB_WriteMany(&circBuf, testIn, BUF_SIZE, true);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);
        CB_PeekMany(&circBuf, testOut, BUF_SIZE);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);
        CB_Remove(&circBuf, BUF_SIZE);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);

        CB_WriteMany(&circBuf, testIn, BUF_SIZE, true);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);
        CB_PeekMany(&circBuf, testOut, BUF_SIZE);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);
        CB_Remove(&circBuf, BUF_SIZE);
        ck_assert(circBuf.readIndex != circBuf.staticSize);
        ck_assert(circBuf.readIndex != BUF_SIZE);
    }
}
END_TEST

Suite *MakeConfigSuite(void)
{
    Suite *s = suite_create("Config");

    // Tests for the Config module
    TCase *tc_core = tcase_create("Config");
    tcase_add_test(tc_core, empty_config);
    tcase_add_test(tc_core, comment_config);
    tcase_add_test(tc_core, comment_newline_config);
    tcase_add_test(tc_core, uart1_input);
    tcase_add_test(tc_core, uart1_baud);
    tcase_add_test(tc_core, uart1_baud_newline);
    tcase_add_test(tc_core, uart1_config);
    tcase_add_test(tc_core, example_file);
    suite_add_tcase(s, tc_core);

    return s;
}

Suite *MakeCircularBufferSuite(void)
{
    Suite *s = suite_create("CircularBuffer");

    // Tests for the Config module
    TCase *tc_core = tcase_create("CircularBuffer");
    tcase_add_test(tc_core, circularbuffer_simple);
    tcase_add_test(tc_core, circularbuffer_complex1);
    tcase_add_test(tc_core, circularbuffer_init);
    tcase_add_test(tc_core, circularbuffer_size_2);
    tcase_add_test(tc_core, circularbuffer_size_max);
    tcase_add_test(tc_core, circularbuffer_remove);
    tcase_add_test(tc_core, circularbuffer_read_write);
    tcase_add_test(tc_core, circularbuffer_readmany);
    tcase_add_test(tc_core, circularbuffer_writemany);
    tcase_add_test(tc_core, circularbuffer_complex2);
    tcase_add_test(tc_core, circularbuffer_complex3);
    suite_add_tcase(s, tc_core);

    return s;
}

 int main(void)
 {
    SRunner *sr = srunner_create(MakeConfigSuite());
    srunner_add_suite(sr, MakeCircularBufferSuite());
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
 }

