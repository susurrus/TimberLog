#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>
#include <stdbool.h>

/**
 * These are possible sources for the UART input data streams.
 * @see ConfigParams
 */
typedef enum {
    PIN_NONE,
    PIN_B2,
    PIN_B3,
    PIN_B10,
    PIN_B11
} Pin;

/**
 * A struct of configuration options.
 * @see ProcessConfigFile
 */
typedef struct {
    uint32_t   uart1BaudRate;
    Pin uart1Input;
    uint32_t   uart2BaudRate;
    Pin uart2Input;
    Pin canRx;
    Pin canTx;
    uint32_t canBaudRate;
    Pin adcInput[2];
    uint32_t adcSampleRate;
} ConfigParams;

// The configuration file cannot be bigger than this (in bytes)
#define MAX_CONFIG_FILE_SIZE 1024

/**
 * Process the configuration file on the current SD card. Assumes all hardware is
 * initialized and the filesystem is ready for reading
 * @param c The next character in the file to parse or NULL if its the end of file
 * @param params The struct to return the parameter values into.
 * @return False if the configuration file was invalid, true otherwise.
 */
bool ProcessConfigFile(char c, ConfigParams *params);

#endif