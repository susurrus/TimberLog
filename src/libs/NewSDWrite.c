#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <spi.h>
#include <xc.h>
#include <ctype.h>

#include "NewSDWrite.h"
#include "Config.h"
#include "Uart2.h"

#include "FSIO.h"

// Specify how many clusters should be allocated at a time. Clusters default to 32KiB.
#define MULTIPLE_CLUSTERS 4

/** Rip some datatypes and function definitions from FSIO.c **/
// Directory entry structure
typedef struct {
    char DIR_Name[DIR_NAMESIZE]; // File name
    char DIR_Extension[DIR_EXTENSION]; // File extension
    uint8_t DIR_Attr; // File attributes
    uint8_t DIR_NTRes; // Reserved byte
    uint8_t DIR_CrtTimeTenth; // Create time (millisecond field)
    uint16_t DIR_CrtTime; // Create time (second, minute, hour field)
    uint16_t DIR_CrtDate; // Create date
    uint16_t DIR_LstAccDate; // Last access date
    uint16_t DIR_FstClusHI; // High word of the entry's first cluster number
    uint16_t DIR_WrtTime; // Last update time
    uint16_t DIR_WrtDate; // Last update date
    uint16_t DIR_FstClusLO; // Low word of the entry's first cluster number
    uint32_t DIR_FileSize; // The 32-bit file size
} _DIRENTRY;
typedef _DIRENTRY * DIRENTRY; // A pointer to a directory entry structure
typedef FSFILE * FILEOBJ;
uint8_t FILEallocate_new_cluster(FSFILE *fo, uint8_t mode);
uint32_t Cluster2Sector(DISK *, uint32_t);
DIRENTRY LoadDirAttrib(FILEOBJ fo, uint16_t *fHandle);
uint32_t WriteFAT(DISK *dsk, uint32_t ccls, uint32_t value, uint8_t forceWrite);
uint8_t Write_File_Entry(FILEOBJ fo, uint16_t *curEntry);
uint8_t flushData (void);
extern uint8_t gNeedFATWrite;
extern uint8_t gNeedDataWrite;
extern DISK gDiskData; // A global datastore for storing disk information. Populated by FSInit().

// Internal functions
static uint8_t Checksum(uint8_t *data, int dataSize);
static bool NewAllocateMultiple(FSFILE *fo);
void CloseLogFile(void);
bool Hex2Nibble(char in, uint8_t *out);

static FSFILE *metaFilePointer = NULL; // A pointer to the current log file
static FSFILE *logFilePointer = NULL; // A pointer to the current log file
static uint32_t lastCluster; // The last cluster number used for the current log file
static uint16_t fileNumber; // The current log number file. Stored in EEPROM.

/**
 * Opens the config file, extracts info, searches for a new filename to use, opens the file for
 * writing. This function assumes that the chip, SD card, and file system are all
 * initialized and the file is ready to be read.
 *
 * TODO: Find previous file used, update it to actual size
 * TODO: Allocate appropriate file size for new entry
 * @return Buad rate extracted from the config file or 0 if invalid
 */
uint16_t OpenNewLogFile(uint16_t lastFileNumber)
{
    // If either the metadata or log file are already open, close them before opening new ones.
    CloseLogFile();

    // Save the file number we're using. We make sure to skip 0xFFFF, as that's an invalid number in
    // the EEPROM, and we wouldn't be able to differentiate from a good log file with that name and
    // no saved log file.
    if (lastFileNumber == INVALID_LOG_NUMBER || lastFileNumber == INVALID_LOG_NUMBER - 1) {
        fileNumber = 0;
    } else {
        fileNumber = lastFileNumber + 1;
    }

    // Open a new meta file
    char metaFileName[] = "0000.meta";
    Uint16ToHex(fileNumber, metaFileName);
    metaFileName[4] = '.'; // Re-add the period that was overwritten by the NUL-terminating character in Uint16ToHex()
    metaFilePointer = FSfopen(metaFileName, FS_WRITE);
    if (!metaFilePointer) {
        return INVALID_LOG_NUMBER;
    }

    // Open a new log file
    char logFileName[] = "0000.log";
    Uint16ToHex(fileNumber, logFileName);
    logFileName[4] = '.'; // Re-add the period that was overwritten by the NUL-terminating character in Uint16ToHex()
    logFilePointer = FSfopen(logFileName, FS_WRITE);
    if (!logFilePointer) {
        return INVALID_LOG_NUMBER;
    }

    // Initialize data for NewSDWriteSector
    logFilePointer->ccls = logFilePointer->cluster;

    // Allocate some clusters
    NewAllocateMultiple(logFilePointer);

    return fileNumber;
}

uint16_t GetLastLogNumberFromCard(void)
{
    SearchRec r;
    if (FindFirst("????.log", ATTR_MASK, &r) == 0) {
        uint16_t lastLog = 0x0000;
        do {
            // Decode the current filename into a log number, saving it if it's the largest found.
            uint16_t logNum;
            if (HexToUint16(r.filename, &logNum)) {
                if (logNum > lastLog) {
                    lastLog = logNum;
                }
            }

        } while (FindNext(&r) == 0);

        return lastLog;
    } else {
        return INVALID_LOG_NUMBER;
    }
}

/**
 * Close the log and metadata file. This should be done before calling OpenNewLogFile() again to
 * prevent running out of file descriptors.
 */
void CloseLogFile(void)
{
    if (metaFilePointer) {
        FSfclose(metaFilePointer);
        metaFilePointer = NULL;
    }
    if (logFilePointer) {
        FSfclose(logFilePointer);
        logFilePointer = NULL;
    }
}

/**
 * Appends the given data to the end of the file. Additionally, if there's no more room in the file,
 * allocate another several clusters worth of space.
 *
 * @param sector A complete sector of data to write.
 * @return True if successful
 */
bool NewSDWriteSector(Sector *sector)
{
    const uint32_t currentSector = Cluster2Sector(logFilePointer->dsk, logFilePointer->ccls) +
        logFilePointer->sec;
    const uint32_t sectorLimit = Cluster2Sector(logFilePointer->dsk, logFilePointer->ccls) +
        logFilePointer->dsk->SecPerClus;

    // add header and footer
    sector->sectorFormat.headerTag = HEADER_TAG;
    sector->sectorFormat.number = fileNumber; // need to figure out how to number these
    sector->sectorFormat.checksum = Checksum(sector->sectorFormat.data,
            sizeof (sector->sectorFormat.data));
    sector->sectorFormat.footerTag = FOOTER_TAG;

    // Write the data
    const bool success = MDD_SDSPI_SectorWrite(currentSector, sector->raw, false);
    if (!success) {
        return false;
    }

    // Check to see if we need to go to a new cluster. Also check for the end of the allocated area
    // allocating another cluster if necessary.
    if (currentSector == sectorLimit - 1) {
        // if this is the last cluster, allocate more
        if (logFilePointer->ccls == lastCluster) {
            if (!NewAllocateMultiple(logFilePointer)) {
                return false;
            }
        }
        // Set cluster and sector to next cluster in our chain
        if (FILEget_next_cluster(logFilePointer, 1) != CE_GOOD) {
            return false;
        }
        logFilePointer->sec = 0;
    } else {
        logFilePointer->sec++;
    }
    // save off the position
    logFilePointer->pos = BYTES_PER_SECTOR - 1; // current position in sector (bytes)

    // save off the seek
    logFilePointer->seek += BYTES_PER_SECTOR; // current position in file (bytes)

    return true;
}

bool LogMetaEvent(const char *eventString, uint32_t timestamp)
{
    char x[128] = {};
    ultoa(x, timestamp, 10);
    size_t xLen = strlen(x);
    x[xLen++] = ':';
    x[xLen++] = ' ';
    strcpy(&x[xLen], eventString);
    xLen = strlen(x);
    x[xLen++] = '\n';

    // Write the string to the file and make sure to flush it to the SD card.
    const size_t bytesWritten = FSfwrite(x, sizeof(char), xLen, metaFilePointer);
    flushData();
    gNeedFATWrite = true;
    NewFileUpdate(metaFilePointer);
    if (bytesWritten < xLen) {
       return false;
    }

    return true;
}

/**
 * Allocates multiple clusters to the given file.
 * @param fo Pointer to the file object to allocate to
 * @return Whether it was successful or not
 */
bool NewAllocateMultiple(FSFILE *fo)
{
    // Save the current cluster of the file
    const uint32_t clusterSave = fo->ccls;

    // Allocate several new clusters
    uint8_t i;
    for (i = 0; i < MULTIPLE_CLUSTERS; i++) {
        FILEallocate_new_cluster(fo, 0);
    }

    // store the last cluster in the file
    lastCluster = fo->ccls;

    // reset current cluster
    fo->ccls = clusterSave;

    // update file size
    fo->size += fo->dsk->SecPerClus * MULTIPLE_CLUSTERS * BYTES_PER_SECTOR;

    // Save all this to the card. We don't need to write any data, just update the FAT table, so
    // we have to set some internal variables for the FSIO library to get this to work.
    gNeedFATWrite = true;
    gNeedDataWrite = FALSE;
    if (NewFileUpdate(logFilePointer)) { // put NewFileUpdates into NewAllocateMultiple
        return true;
    } else {
        return false;
    }
}

/**
 * Mimics FSfileClose to write file info to the SD card.
 * @param fo The file to update
 * @return If it was successful
 */
bool NewFileUpdate(FSFILE *fo)
{
    if (fo == NULL) {
        return 0;
    }
    uint16_t fHandle = fo->entry;

    // Write the file data
    WriteFAT(fo->dsk, 0, 0, true);

    // Update file entry data
    DIRENTRY dir = LoadDirAttrib(fo, &fHandle);
    if (dir == NULL) {
        return false;
    }
    dir->DIR_FileSize = fo->size;
    dir->DIR_Attr = fo->attributes;
    dir->DIR_FstClusHI = (fo->cluster & 0xFFFF0000) >> 16;
    dir->DIR_FstClusLO = fo->cluster & 0x0000FFFF;

    Write_File_Entry(fo, &fHandle);

    return true;
}

uint32_t GetSectorSize(void)
{
    return gDiskData.sectorSize;
}

/**
 * Creates a two byte checksum - used for the sector checksum
 * return format : [odds][evens]
 *                 15          0
 * @param data A pointer to the data to checksum
 * @param dataSize The size of the data
 * @return the checksum
 */
uint8_t Checksum(uint8_t * data, int dataSize)
{
    int i;
    uint8_t sum = 0;
    for (i = 0; i < dataSize; i++) {
        sum ^= data[i];
    }
    return sum;
}

/**
 * Convert an unsigned integer to a 4-character hexadecimal ASCII string.
 * @param x The number to convert
 * @param out[out] The output string.
 */
void Uint16ToHex(uint16_t x, char out[5])
{
   const char hexDigits[] = "0123456789ABCDEF";
   out[3] = hexDigits[x & 0xF];
   out[2] = hexDigits[(x >> 4) & 0xF];
   out[1] = hexDigits[(x >> 8) & 0xF];
   out[0] = hexDigits[(x >> 12) & 0xF];
   out[4] = '\0';
}

bool Hex2Nibble(char in, uint8_t *out)
{
    if ('a' <= in && in <= 'f') {
        *out = in - 'a' + 0xA;
        return true;
    } else if ('A' <= in && in <= 'F') {
        *out = in - 'A' + 0xA;
        return true;
    } else if ('0' <= in && in <= '9') {
        *out = in - '0';
        return true;
    }

    return false;
}

bool HexToUint16(const char in[4], uint16_t *out)
{
    *out = 0x0000;
    uint8_t nibble;

    if (Hex2Nibble(in[3], &nibble)) {
        *out |= nibble;
    } else {
        return false;
    }
    if (Hex2Nibble(in[2], &nibble)) {
        *out |= nibble << 4;
    } else {
        return false;
    }
    if (Hex2Nibble(in[1], &nibble)) {
        *out |= nibble << 8;
    } else {
        return false;
    }
    if (Hex2Nibble(in[0], &nibble)) {
        *out |= nibble << 12;
    } else {
        return false;
    }

    return true;
}