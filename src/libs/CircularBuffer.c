/*
 * Copyright Bar Smith, Bryant Mairs 2012
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses.
 */
 
/**
 * @file   CircularBuffer.c
 * @author Bar Smith
 * @author Bryant Mairs
 * @date   August, 2012
 * @brief  Provides a circular buffer implementation for bytes and non-primitive datatypes.
 *
 * This circular buffer provides a single buffer interface for almost any situation necessary. It
 * has been written for use with the dsPIC33f, but has been tested on x86.
 *
 * Unit testing has been completed on x86 by compiling with the UNIT_TEST_CIRCULAR_BUFFER macro.
 * With gcc: `gcc CircularBuffer.c -DUNIT_TEST_CIRCULAR_BUFFER -Wall -g`
 */
#include "CircularBuffer.h"

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

int CB_Init(CircularBuffer *b, uint8_t *buffer, const uint16_t size)
{
	// Check the validity of pointers.
	if (!buffer || !b) {
		return false;
	}

	// Checks that the size is valid.
	if (size <= 1) {
		return false;
	}

	// Store the buffer pointer and initialize it all to zero.
	// This is not necessary, but makes debugging easier.
	b->data = buffer;
	uint16_t i;
	for (i = 0; i < size; ++i) {
		b->data[i] = 0;
	}

	// Initialize all variables. The only one of note is `empty`, which is initialized to true.
	b->readIndex = 0;
	b->writeIndex = 0;
	b->staticSize = size;
	b->dataSize = 0;
	b->overflowCount = 0;

	return true;
}

int CB_ReadByte(CircularBuffer *b, uint8_t *outData)
{
	if (b) {
		if (b->dataSize) {
			//copys the last element from the buffer to data
			*outData = b->data[b->readIndex];
			//sets the buffer empty if there was only one element in it
			if (b->dataSize == 1) {
				//checks for wrap around
				b->readIndex = b->readIndex < (b->staticSize - 1)?b->readIndex + 1:0;
			} else {
				//checks for wrap around and moves indicies
				b->readIndex = b->readIndex < (b->staticSize - 1)?b->readIndex + 1:0;
			}
			--b->dataSize;
			return true;
		}
	}
	return false;
}

int CB_ReadMany(CircularBuffer *b, void *outData, uint16_t size)
{
	int16_t i;
	if (b && outData) {
		//cast data so that it can be used to ready bytes
		uint8_t *data_u = (uint8_t*)outData;
		//check if there are enough items in the buffer to read
		if (b->dataSize >= size) {

			// And read the data.
			for (i = 0; i < size; ++i) {
				data_u[i] = b->data[b->readIndex];

				// Update the readIndex taking into account wrap-around.
				if (b->readIndex < b->staticSize - 1) {
					++b->readIndex;
				} else {
					b->readIndex = 0;
				}
			}
			b->dataSize -= size;
			return true;
		}
	}
	return false;
}

int CB_WriteByte(CircularBuffer *b, uint8_t inData)
{
	if (b) {
		// If the buffer is full the overflow count is incremented and no data is written.
		if (b->dataSize == b->staticSize) {
			++b->overflowCount;
			return false;
		} else {
			b->data[b->writeIndex] = inData;
			// Now update the writeIndex taking into account wrap-around.
			b->writeIndex = b->writeIndex < (b->staticSize - 1) ? b->writeIndex + 1: 0;
			++b->dataSize;
			return true;
		}
	}
	return false;
}

int CB_WriteMany(CircularBuffer *b, const void *inData, uint16_t size, bool failEarly)
{
	if (b && inData) {
		uint8_t *data_u = (uint8_t*)inData;
		//if the fail early value is set
		if (failEarly) {
			//Checks to make sure there is enough space
			if (b->staticSize - b->dataSize < size) {
				return false;
			} else {
				int i = 0;
				//runs size times
				while (i < size) {
					//writes to the buffer
					b->data[b->writeIndex] = data_u[i];
					++i;
					//checks for wrap around and moves the indicies
					b->writeIndex = b->writeIndex < (b->staticSize - 1) ? b->writeIndex + 1: 0;
				}
				b->dataSize += i;
				return true;
			}
		}
		// Otherwise we try and write as much data as we can.
		else {
			int i = 0;
			while (i < size) {
				//if the buffer is full the overflow count is increased and false is returned
				if (b->dataSize == b->staticSize) {
					b->overflowCount += (size - i);
					return false;
				}
				//reads an element from the buffer to data
				b->data[b->writeIndex] = data_u[i];
				++i;
				++b->dataSize;
				//move the indicies and check for wrap around
				b->writeIndex = (b->writeIndex < (b->staticSize - 1)) ? b->writeIndex + 1: 0;
			}
			return true;
		}
	}
	return false;
}

int CB_Peek(const CircularBuffer *b, uint8_t *outData)
{
	if (b) {
		if (b->dataSize > 0) {
			*outData = b->data[b->readIndex];
			return true;
		}
	}
	return false;
}

int CB_PeekMany (const CircularBuffer *b, void *outData, uint16_t size)
{
	uint16_t i;
	int tmpHead;

	if (b) {
		uint8_t *data_u = (uint8_t*)outData;
		// Make sure there's enough data to read off and read them off one-by-one.
		if (b->dataSize >= size) {
			tmpHead = b->readIndex;
			for (i = 0; i < size; ++i) {
				data_u[i] = b->data[tmpHead];

				// Handle wrapping around the buffer.
				if (tmpHead < b->staticSize - 1) {
					++tmpHead;
				} else {
					tmpHead = 0;
				}
			}
			return true;
		}
	}
	return false;
}

int CB_Remove(CircularBuffer *b, uint16_t size){
	// If there are enough elements to remove.
	if (b->dataSize >= size) {
		// Checks to see if the buffer will wrap around.
		if ((b->staticSize - b->readIndex) <= size) {
			b-> readIndex = b->readIndex + size - b->staticSize;
		} else {
			// If the buffer will not wrap around size is added to read index.
			b-> readIndex = b->readIndex + size;
		}
		b->dataSize -= size;
		return true;
	}
	// If one is trying to remove more elements than are in the buffer, the buffer is made empty.
	else {
		b->readIndex = b->writeIndex;
		b->dataSize = 0;
		return false;
	}
}