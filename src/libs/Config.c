#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "Config.h"

#define MAX_PARAM_NAME_SIZE 25
#define MAX_PARAM_VALUE_SIZE 25

#define IS_WHITESPACE(c) ((c) == ' ' || (c) == '\t')
#define IS_TEXT(c) (isalnum(c) || (c) == '_')
#define IS_NEWLINE(c) ((c) == '\r' || (c) == '\n')
#define IS_COMMENT(c) ((c) == '#')
#define IS_EOF(c) ((c) == '\0')

/**
 * Describes the various states of the config file parser. The main gist is that each line is parsed
 * individually and contains a parameter name and value. There's extra handling for arbitrary
 * whitespace and comments can exist on blank lines or after param/value lines.
 */
typedef enum {
    CONFIG_PROCESSING_STATE_LINE_START, // Initial processing state that indicates the start of the line.
    CONFIG_PROCESSING_STATE_INITIAL_WHITESPACE, // Processing the whitespace at the start of the line.
    CONFIG_PROCESSING_STATE_PARAM_NAME, // Processing the parameter name (alphanumeric and '_')
    CONFIG_PROCESSING_STATE_WHITESPACE_BEFORE_VALUE, // Processing whitespace afterwards.
    CONFIG_PROCESSING_STATE_PARAM_VALUE, // Processing the parameter value (alphanumeric and '_')
    CONFIG_PROCESSING_STATE_WHITESPACE_AFTER_VALUE, // Processing any whitespace afterwards.
    CONFIG_PROCESSING_STATE_COMMENT, // Processing a comment (runs until newline)
} ConfigProcessingState;

bool ProcessParamNameValuePair(ConfigParams *params, const char *name, const char *value) {
    // Now that we have a parameter/value pair, process them into the output configuration
    // parameter struct.
    if (strcmp(name, "uart1_input") == 0) {
        if (strcmp(value, "b2") == 0) {
            params->uart1Input = PIN_B2;
        } else if (strcmp(value, "b3") == 0) {
            params->uart1Input = PIN_B3;
        } else if (strcmp(value, "b10") == 0) {
            params->uart1Input = PIN_B10;
        } else if (strcmp(value, "b11") == 0) {
            params->uart1Input = PIN_B11;
        } else {
            return false;
        }
    } else if (strcmp(name, "uart1_baud") == 0) {
        params->uart1BaudRate = strtoul(value, NULL, 10);
        if (!params->uart1BaudRate) {
            return false;
        }
    } else if (strcmp(name, "uart2_input") == 0) {
        if (strcmp(value, "b2") == 0) {
            params->uart2Input = PIN_B2;
        } else if (strcmp(value, "b3") == 0) {
            params->uart2Input = PIN_B3;
        } else if (strcmp(value, "b10") == 0) {
            params->uart2Input = PIN_B10;
        } else if (strcmp(value, "b11") == 0) {
            params->uart2Input = PIN_B11;
        } else {
            return false;
        }
    } else if (strcmp(name, "uart2_baud") == 0) {
        params->uart2BaudRate = strtoul(value, NULL, 10);
        if (!params->uart2BaudRate) {
            return false;
        }
    } else if (strcmp(name, "can_tx") == 0) {
        if (strcmp(value, "b3") == 0) {
            params->canTx = PIN_B3;
        } else if (strcmp(value, "b10") == 0) {
            params->canTx = PIN_B10;
        } else if (strcmp(value, "b11") == 0) {
            params->canTx = PIN_B11;
        } else {
            return false;
        }
    } else if (strcmp(name, "can_rx") == 0) {
        if (strcmp(value, "b2") == 0) {
            params->canRx = PIN_B2;
        } else if (strcmp(value, "b3") == 0) {
            params->canRx = PIN_B3;
        } else if (strcmp(value, "b10") == 0) {
            params->canRx = PIN_B10;
        } else if (strcmp(value, "b11") == 0) {
            params->canRx = PIN_B11;
        } else {
            return false;
        }
    } else if (strcmp(name, "can_baud") == 0) {
        params->canBaudRate = strtoul(value, NULL, 10);
        if (!params->canBaudRate) {
            return false;
        }
    } else if (strcmp(name, "adc_input") == 0) {
        if (strcmp(value, "b2") == 0) {
            params->canRx = PIN_B2;
        } else if (strcmp(value, "b3") == 0) {
            params->canRx = PIN_B3;
        }
        // TODO: Add support for both b2 & b3
        else {
            return false;
        }
    } else {
        return false;
    }
    
    return true;
}

bool ProcessConfigFile(char c, ConfigParams *params)
{
    static ConfigProcessingState state;
    static char paramName[MAX_PARAM_NAME_SIZE];
    static int paramNameIndex = 0;
    static char paramValue[MAX_PARAM_VALUE_SIZE];
    static int paramValueIndex = 0;

    // We track the start and end of each line and iterate until we run off the end of it.
    switch (state) {
        case CONFIG_PROCESSING_STATE_LINE_START:
            // First we skip any additional newline characters (effectively empty rows)
            if (IS_NEWLINE(c)) {
                state = CONFIG_PROCESSING_STATE_LINE_START;
                return true; // This is necessary to prevent fall-through if this if-statement is entered.
            }
            // break purposefully missing here
        // Here we're scanning for either whitespace, a parameter name, or a comment
        case CONFIG_PROCESSING_STATE_INITIAL_WHITESPACE:
            if (IS_WHITESPACE(c)) {
                state = CONFIG_PROCESSING_STATE_INITIAL_WHITESPACE;
            } else if (IS_TEXT(c)) {
                paramNameIndex = 0;
                paramName[paramNameIndex++] = c;
                state = CONFIG_PROCESSING_STATE_PARAM_NAME;
            } else if (IS_COMMENT(c)) {
                state = CONFIG_PROCESSING_STATE_COMMENT;
            } else if (IS_EOF(c)) {
                state = CONFIG_PROCESSING_STATE_LINE_START;
            } else {
                state = CONFIG_PROCESSING_STATE_LINE_START;
                return false;
            }
            break;
        // Scanning for the name just entails waiting for the end.
        case CONFIG_PROCESSING_STATE_PARAM_NAME:
            // If we keep finding value name characters, keep going
            if (IS_TEXT(c)) {
                paramName[paramNameIndex++] = c;
                state = CONFIG_PROCESSING_STATE_PARAM_NAME;
            }
            // But if we find whitespace, we're done
            else if (IS_WHITESPACE(c)) {
                // Null-terminate the parameter name
                paramName[paramNameIndex++] = '\0';
                state = CONFIG_PROCESSING_STATE_WHITESPACE_BEFORE_VALUE;
            }
            // And if we find anything else, we error out.
            else {
                state = CONFIG_PROCESSING_STATE_LINE_START;
                return false;
            }
            break;
        // Now we're searching for whitespace again until we find the parameter value
        case CONFIG_PROCESSING_STATE_WHITESPACE_BEFORE_VALUE:
            if (IS_WHITESPACE(c)) {
                state = CONFIG_PROCESSING_STATE_WHITESPACE_BEFORE_VALUE;
            } else if (IS_TEXT(c)) {
                paramValueIndex = 0;
                paramValue[paramValueIndex++] = c;
                state = CONFIG_PROCESSING_STATE_PARAM_VALUE;
            } else {
                state = CONFIG_PROCESSING_STATE_LINE_START;
                return false;
            }
            break;
        // Scanning for the value just entails waiting for the end.
        case CONFIG_PROCESSING_STATE_PARAM_VALUE:
            // If we keep finding value name characters, keep going
            if (IS_TEXT(c)) {
                paramValue[paramValueIndex++] = c;
                state = CONFIG_PROCESSING_STATE_PARAM_VALUE;
            }
            // But if we find whitespace, we're done
            else if (IS_WHITESPACE(c)) {
                // Null-terminate the parameter value
                paramValue[paramValueIndex++] = '\0';
                if (!ProcessParamNameValuePair(params, paramName, paramValue)) {
                    state = CONFIG_PROCESSING_STATE_LINE_START;
                    return false;
                }
                state = CONFIG_PROCESSING_STATE_WHITESPACE_AFTER_VALUE;
            }
            // Newlines also indicate we're done
            else if (IS_NEWLINE(c)) {
                paramValue[paramValueIndex++] = '\0';
                if (!ProcessParamNameValuePair(params, paramName, paramValue)) {
                    state = CONFIG_PROCESSING_STATE_LINE_START;
                    return false;
                }
                state = CONFIG_PROCESSING_STATE_LINE_START;
            }
            // Same with comments
            else if (IS_COMMENT(c)) {
                paramValue[paramValueIndex++] = '\0';
                if (!ProcessParamNameValuePair(params, paramName, paramValue)) {
                    state = CONFIG_PROCESSING_STATE_LINE_START;
                    return false;
                }
                state = CONFIG_PROCESSING_STATE_COMMENT;
            }
            // Or the end of the file
            else if (IS_EOF(c)) {
                paramValue[paramValueIndex++] = '\0';
                if (!ProcessParamNameValuePair(params, paramName, paramValue)) {
                    state = CONFIG_PROCESSING_STATE_LINE_START;
                    return false;
                }
                state = CONFIG_PROCESSING_STATE_LINE_START;
            }
            // But if we find anything else, we error out.
            else {
                state = CONFIG_PROCESSING_STATE_LINE_START;
                return false;
            }
            break;
        // Now we're searching for whitespace again until a comment or end of line
        case CONFIG_PROCESSING_STATE_WHITESPACE_AFTER_VALUE:
            // Keep chugging through whitespace
            if (IS_WHITESPACE(c)) {
                state = CONFIG_PROCESSING_STATE_WHITESPACE_AFTER_VALUE;
            }
            // Newlines indicate we're done
            else if (IS_NEWLINE(c)) {
                state = CONFIG_PROCESSING_STATE_LINE_START;
            }
            // Same with comments
            else if (IS_COMMENT(c)) {
                state = CONFIG_PROCESSING_STATE_COMMENT;
            }
            // Or the end of the file, denoted by the null character
            else if (IS_EOF(c)) {
                state = CONFIG_PROCESSING_STATE_LINE_START;
            }
            // But if we find anything else, we error out.
            else {
                state = CONFIG_PROCESSING_STATE_LINE_START;
                return false;
            }
            break;
        // Comments can contain anything excluding newline characters
        case CONFIG_PROCESSING_STATE_COMMENT:
            if (IS_NEWLINE(c)) {
                state = CONFIG_PROCESSING_STATE_LINE_START;
            }
            // Or the end of the file, denoted by the null character
            else if (IS_EOF(c)) {
                state = CONFIG_PROCESSING_STATE_LINE_START;
            }
            break;
    }

    return true;
}